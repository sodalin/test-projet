<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 10/03/18
 * Time: 11:06
 */

require_once 'models/Invoice.php';
use PHPUnit\Framework\TestCase;

class InvoiceTest extends TestCase {
    public function testAmountHT() {
        // Ce test doit retourner un montant total de 15.99€
        $invoice = new Invoice('Ma première facture');
        $line1 = new InvoiceLine('1 burger', 12.45);
        $line2 = new InvoiceLine('2 bières', 14);
        $invoice->addLines($line1);
        $invoice->addLines($line2);
        

        $this->assertEquals(26.45, $invoice->getAmountHT());
        // $this->assertNotEmpty($subscriberList);
    }

    public function testTitle() {
    $invoice = new Invoice('Ma première facture');

     $this->assertEquals('Ma première facture', $invoice->getTitle());

    }

    public function testId() {
        $invoice = new Invoice('Ma première facture');
        $invoice->setId(1567354);
        $this->assertInternalType('int', $invoice->getId());
    
    }

    public function testNbLinesInvoice() {
        $invoice = new Invoice('Ma première facture');
        $line1 = new InvoiceLine('1 burger', 12.45);
        $line2 = new InvoiceLine('2 bières', 14);
        $invoice->addLines($line1);
        $invoice->addLines($line2);
        $this->assertCount(2, $invoice->getAllLines());

    }

    public function testLinesInvoice() {
        $invoice = new Invoice('Ma première facture');
        $line1 = new InvoiceLine('1 burger', 12.45);
        $invoice->addLines($line1);
        $this->assertNotNull($invoice->getLine(0));

    }

    // public function testAmountHT() {
    //     $invoice = new Invoice('Ma première facture');
    //     $line1 = new InvoiceLine('1 burger', 12.45);
    //     $line2 = new InvoiceLine('2 bières', 14);
    //     $invoice->addLines($line1);
    //     $invoice->addLines($line2);
    //     $this->assertEquals(26.45, $invoice->getAmountHT());


        
    // }
    
    // public function testAmountTTC() {
    //     $invoice = new Invoice('Ma première facture');
    //     $line1 = new InvoiceLine('1 burger', 12.45);
    //     $invoice->addLines($line1);
    //     $this->assertNotNull($invoice->getLine(0));

    // }
    


}


