<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 11/03/18
 * Time: 20:52
 */

require_once 'InvoiceLine.php';

class Invoice {
    private $id = null;
    private $title = null;
    private $lines = array();

    public function __construct($title) {
        $this->title = $title;
        $this->setId(mt_rand(100000, 999999));
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getLine($num) {
        return $this->lines[$num];
    }

    public function getAllLines() {
        return $this->lines;
    }

    public function addLines($lines) {
        $this->lines[] = $lines;
    }

    public function getAmountHT() {
        $amount = 0;
        foreach($this->lines as $line) {
            $amount += $line->getAmountHT();
        }

        return $amount;
    }

    public function getAmountTTC() {
        $amount = 0;
        foreach($this->lines as $line) {
            $amount += $line->getAmountHT();
        }

        return $amount;
    }

    public function removeLine($id) {
        foreach($this->lines as $key => $line) {
            if ($line->getId() == $id) {
                unset($this->lines[$key]);
                break;
            }
        }
    }
}