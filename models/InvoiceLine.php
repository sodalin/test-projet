<?php

/**
 * Created by PhpStorm.
 * User: gchanteloube
 * Date: 11/03/18
 * Time: 20:56
 */
class InvoiceLine {
    private $id = null;
    private $title = null;
    private $amountHT = null;
    private $tva = null;

    public function __construct($title, $amount = 0, $tva = 20) {
        $this->title = $title;
        $this->amountHT = $amount;
        $this->setTva($tva);
        $this->setId(mt_rand(100000, 999999));
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getAmountHT() {
        return $this->amountHT;
    }

    public function setAmountHT($amountHT) {
        $this->amountHT = $amountHT;
    }

    public function getTva() {
        return $this->tva;
    }

    public function setTva($tva) {
        $this->tva = $tva;
    }
}